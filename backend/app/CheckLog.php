<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckLog extends Model {
	protected $table = 'check_logs';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['officer_id', 'check_schedule_id',
	'check_when','vdo_path', 'lat','long'];

	
	public function checkSchedule()
	{
		return $this->belongsTo('CheckSchedule');
	}
	public function officer()
	{
		return $this->belongsTo('Officer');
	}
}

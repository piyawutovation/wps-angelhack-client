<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckPoint extends Model {
	protected $table = 'check_points';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['check_point_name', 'project_id', 'lat','long'];

	public function project()
	{
		return $this->belongsTo('Project');
	}

	public function checkSchedules()
	{
		return $this->hasMany('CheckSchedule');
	}
}

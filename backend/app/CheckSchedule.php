<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckSchedule extends Model {
	protected $table = 'check_schedules';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['officer_id', 'check_when', 
	'check_point_id'];

	public function Officer()
	{
		return $this->belongsTo('Officer');
	}
	public function CheckPoint()
	{
		return $this->belongsTo('CheckPoint');
	}
	public function checkLogs()
	{
		return $this->hasMany('CheckLog');
	}//
}

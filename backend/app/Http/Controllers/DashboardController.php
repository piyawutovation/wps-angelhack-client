<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Project;

class DashboardController extends Controller
{
    /**
     * Show all project
     *
     * @return Response
     */
    public function showAllProjects()
    {
        $projects = Project::all();
        return view('dashboard', ['projects' => Project::all()]);
    }
}
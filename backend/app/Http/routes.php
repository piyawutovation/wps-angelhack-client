<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
*/
use App\Officer;
use App\CheckLog;
use App\Project;

Route::get('/',function(){
	return Response::json(Officer::all());
});
Route::get('post',function(){
	return View::make('checklog_post');
});

Route::post('/api/forguard/v1/checkin', function(){
	$CheckLog = new CheckLog();
	$CheckLog->officer_id = Input::get('officer_id');
	$CheckLog->check_schedule_id = Input::get('check_schedule_id');

	if (!empty(Input::file('photo_file')) && Input::file('photo_file')->isValid()) {
		$destinationPath = 'checklog_pictures'; // upload path
	  	$extension = Input::file('photo_file')->getClientOriginalExtension(); // getting image extension
      	$fileName = time(). '_' . $CheckLog->officer_id . '.' . $extension;
      	Input::file('photo_file')->move($destinationPath, $fileName); // uploading file to given path
      	$CheckLog->vdo_path = $destinationPath . '/' . $fileName;
	}

	$CheckLog->check_when = date("Y-m-d H:i:s", strtotime(Input::get('time')));
	$CheckLog->lat = Input::get('gps_latitude');
	$CheckLog->long = Input::get('gps_longitute');
	$CheckLog->save();

	return Response::json([
		'success' => true
	]);
});

Route::get('checkin', function() {
	return Response::json(CheckLog::all());
});

Route::post('officer/checkin_nopic',function(){
	$CheckLog = new CheckLog();
	$CheckLog->officer_id = Input::get('officer_id');
	$CheckLog->check_schedule_id = Input::get('check_schedule_id');
	$CheckLog->check_when = Input::get('check_when');
	$CheckLog->vdo_path = Input::get('vdo_path');
	$CheckLog->lat = Input::get('lat');
	$CheckLog->long = Input::get('long');
	$CheckLog->save();
});

/**
 * Listing all officer
 */
Route::get('officer', function() {
	return Response::json(Officer::all());
});


function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

/**
 * Project status
 */
Route::get('projects/status', function() {

	$result = array();


	if (CheckLog::all()->count() > 0) {
		array_push($result, 'Angel HACK');
	}
	// $has = [];

	// $projects = Project::all();

	// $checklogs = CheckLog::all();
	// $checklogs->each(function() {

	// });

	// $projects->each(function(){

	// });
	// for ($i = 0; $i < count($projects); $i++) {
	// 	$project = $projects[$i];
	// 	$checklogs = CheckLog::all();

	// 	for ($j = 0; $j < count($checklogs); $j++) {
	// 		$checklog = $checklogs[$j];
	// 		var_dump($checklog);
	// 		//$distance = distance($project[$i]->lat, $project[$i]->long, $checklog[$j]->lat, $checklog[$j]->long, "K");
	// 		//echo $distance;
	// 	}
	// }

	return Response::json($result);
});

Route::get('checkin/clear', function() {
	DB::statement("SET foreign_key_checks=0");
	CheckLog::truncate();
	DB::statement("SET foreign_key_checks=1");
});

Route::get('projects/status_v3', function() {
	return Response::json([
		'project_ids' => array(5),
	]);
});

/**
 * Display officer post form test
 */
Route::get('officer/post', function() {
	return view('officer.new');
});

/**
 * Add new officer
 */
Route::post('officer/new', function() {
	$officer = new Officer();
	$officer->officer_name = Input::get('officer_name');
	$officer->surname = Input::get('surname');
	$officer->nickname = Input::get('nickname');
	$officer->birthdate = Input::get('birthdate');
	$officer->salary = Input::get('salary');

	if (!empty(Input::file('image')) && Input::file('image')->isValid()) {
      $destinationPath = 'uploads'; // upload path
      $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
      $officer->imagepath = $destinationPath . '/' . $fileName;
    }

	$officer->project_id = Input::get('project_id');
	$officer->save();
	return Redirect::to('officer');
});

Route::get('dashboard', 'DashboardController@showAllProjects');
<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model {
	protected $table = 'officers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['officer_name', 'surname', 
	'nickname','birthdate','salary','imagepath','project_id'];

	public function project()
	{
		return $this->belongsTo('Project');
	}

}

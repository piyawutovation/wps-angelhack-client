<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects',function(Blueprint $table){
			$table->increments('id');
			$table->string('project_name');
			$table->string('address');
			$table->double('lat',10,6);
			$table->double('long',10,6);
			$table->timestamps();
		});


		Schema::create('officers',function(Blueprint $table){
			$table->increments('id');
			$table->string('officer_name');
			$table->string('surname');
			$table->string('nickname');
			$table->date('birthdate');
			$table->decimal('salary');
			$table->string('imagepath');
			$table->integer('project_id')->unsigned();
			$table->foreign('project_id')->references('id')->on('projects');
			$table->timestamps();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('officers');
		Schema::drop('projects');
	}

}

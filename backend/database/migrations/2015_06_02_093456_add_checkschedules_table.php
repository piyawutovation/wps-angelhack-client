<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckschedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('check_schedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('officer_id')->unsigned();
			$table->foreign('officer_id')->references('id')->on('officers');
			$table->datetime('check_when');
			$table->integer('check_point_id')->unsigned();
			$table->foreign('check_point_id')->references('id')->on('check_points');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('check_schedules');
	}

}

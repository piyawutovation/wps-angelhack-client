<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChecklogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('check_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('officer_id')->unsigned();
			$table->foreign('officer_id')->references('id')->on('officers');
			$table->integer('check_schedule_id')->unsigned();
			$table->foreign('check_schedule_id')->references('id')->on('check_schedules');
			$table->datetime('check_when');
			$table->string('vdo_path');
			$table->double('lat',10,6);
			$table->double('long',10,6);
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('check_logs');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->call('ProjectTableSeeder');
		$this->call('OfficerTableSeeder');
		$this->call('CheckPointTableSeeder');
		$this->call('CheckScheduleTableSeeder');
		$this->call('CheckLogTableSeeder');

		
	}

}
class UserTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert(array(array(
			'id' => 1,
			'name' => 'firstuser',
			'password' => 'firstpassword',
			'email' =>'test@test.com',
			'remember_token' => 'token',
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('UserTableSeeder seeded!');
	}
}
class ProjectTableSeeder extends Seeder
{
	public function run()
	{

		DB::table('projects')->insert(array(array(
			'id' => 1,
			'project_name'=>'building',
			'address'=>'somewhere',
			'lat'=>10.0,
			'long'=>10.1,
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('ProjectTableSeeder seeded!');
	}
}
class OfficerTableSeeder extends Seeder
{
	public  function run()
	{

		DB::table('officers')->insert(array(array(
			'id' => 1,
			'officer_name' => 'testname',
			'surname' => 'testsurname',
			'nickname' => 'tset',
			'birthdate' => new DateTime,
			'salary' => 10000,
			'imagepath' => '',
			'project_id' => 1,
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('OfficerTableSeeder seeded!');

	}
}
class CheckPointTableSeeder extends Seeder
{
	public  function run()
	{

		DB::table('check_points')->insert(array(array(
			'id' => 1,
			'check_point_name' => 'checkpointname',
			'project_id' => 1,
			'lat'=>10.0,
			'long'=>10.1,			
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('CheckPointTableSeeder seeded!');
	}
}
class CheckScheduleTableSeeder extends Seeder
{
	public  function run()
	{
		DB::table('check_schedules')->insert(array(array(
			'id' => 1,
			'officer_id' => 1,
			'check_when' => new DateTime,
			'check_point_id' => 1,
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('CheckScheduleTableSeeder seeded!');
	}
}
class CheckLogTableSeeder extends Seeder
{
	public  function run()
	{

		DB::table('check_logs')->insert(array(array(
			'id' => 1,
			'officer_id' => 1,
			'check_schedule_id' => 1,
			'check_when' => new DateTime,
			'vdo_path' => '',
			'lat'=>10.0,
			'long'=>10.1,						
			'created_at' => new DateTime,
			'updated_at' => new DateTime
		)));
		$this->command->info('CheckLogTableSeeder seeded!');
	}
}

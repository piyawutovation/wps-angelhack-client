<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>Waller Dashboard</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"
            type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.12/gmaps.min.js"></script>
  </head>

  <body style="margin:0px; padding:0px;">
    <div id="map" style="width: 100%; height: 100%"></div>
    <script type="text/javascript">
    $(function(){
      var map;
      var markers = [];
      var infoWindow;
      var locationSelect;
      var projects = {!! $projects !!};
      map = new GMaps({
        div: '#map',
        lat: 13.7369404,
        lng: 100.572686,
        zoom: 13
      });

      // Sets the map on all markers in the array.
      var setAllMap = function(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      var clearMarkers = function() {
        setAllMap(null);
      }

      var refreshMap = function(status) {

        for (var i = 0, totalProject = projects.length; i < totalProject; i++) {

          var markerUrl = '/images/red_marker.png';

          if (status && status.length > 0 && projects[i].id === 5) {
            markerUrl = '/images/green_marker.png'
          }

          var marker = map.createMarker({
              lat: projects[i].lat,
              lng: projects[i].long,
              title: projects[i].project_name,
              infoWindow: {
                content: '<p><b>' + projects[i].project_name + '</b></p>'
              },
              icon: {
                url: markerUrl,
                size: new google.maps.Size(20, 34),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(10, 34),
                scaledSize: new google.maps.Size(20, 34)
              }
          });

          markers.push(marker);
          map.addMarker(marker);
        }
      }

      setInterval(function() {
        $.get('/projects/status', function(data) {
          clearMarkers();
          refreshMap(data);
        });
      }, 5000);

      refreshMap();
    });
    </script>
  </body>
</html>

package com.example.admin.wps;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    int selectedPosition = -1;
    Location lastLocation;
    TextView locationTextView;


    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ListView listView = (ListView)findViewById(R.id.listView);
        GuardAdapter guardAdapter = new GuardAdapter(this);
        listView.setAdapter(guardAdapter);
        final Context context = this;
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        System.err.println("yeah click " + position);

                        selectedPosition = position;


                        // the real case
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 0);

                    }
                }
        );


        // setting up the location service
        MyLocationListener locationListener = new MyLocationListener();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        locationTextView = (TextView) findViewById(R.id.locationTextView);
    }

    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            lastLocation = location;
            locationTextView.setText("พิกัด : "+ lastLocation.getLatitude() + ", " + lastLocation.getLongitude() +"");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // finish camera work
        Bitmap bp = (Bitmap) data.getExtras().get("data");

        Intent intent = new Intent(this, UploadActivity.class);
        intent.putExtra("guardId", selectedPosition);
        intent.putExtra("capturedBitmap", bp);
        if (lastLocation != null) {
            intent.putExtra("latitude", lastLocation.getLatitude());
            intent.putExtra("longitude", lastLocation.getLongitude());
        }
        else {
            intent.putExtra("latitude", 0.0);
            intent.putExtra("longitude", 0.0);
        }
        this.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}



class GuardAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;


    GuardAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return "Test item" + position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.single_row, null);

        TextView rowTitle = (TextView) vi.findViewById(R.id.rowTitle);
        TextView rowDescription = (TextView) vi.findViewById(R.id.rowDescription);
        ImageView rowImage=(ImageView)vi.findViewById(R.id.rowImage); // thumb image


        String[] officerNames = {"นายเฮง","นายชัชชาติ","นายสิทธิ","นายจอมพลัง"};
        String[] descriptions = {"ครั้งสุดท้าย 21.00","ครั้งสุดท้าย เมื่อวาน","เพิ่งตรวจ","ไม่ต้องตรวจ"};

        rowTitle.setText( officerNames[position]);
        rowDescription.setText(descriptions[position]);
        int[] imageIds = {R.drawable.y1, R.drawable.y2, R.drawable.y3, R.drawable.y4};
        rowImage.setImageResource(imageIds[position % imageIds.length]);

        return vi;
    }
}
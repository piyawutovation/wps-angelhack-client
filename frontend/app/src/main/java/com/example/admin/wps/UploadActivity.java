package com.example.admin.wps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;


public class UploadActivity extends ActionBarActivity {

    private ProgressBar pb;
    // private final String filename = "/mnt/sdcard/vid.mp4";
    // private final String filename = "/mnt/sdcard/a.3gp";
    private String urlString = "http://doctorlog-piyawutovation.c9.io/public/robots";
    private TextView tv;
    long totalSize = 0;

    Button finishUploadButton;

    Bitmap bitmapToUp;

    private final OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploading);


        pb = (ProgressBar) findViewById(R.id.uploadProgressBar);
        tv = (TextView) findViewById(R.id.progressTextView);

        bitmapToUp = BitmapFactory.decodeResource(getResources(),
                R.drawable.guard1);

        finishUploadButton = (Button) findViewById(R.id.finishUploadButton);
        finishUploadButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.err.println("we : OK!!!");
                        goHome();
                    }
                }
        );
        finishUploadButton.setVisibility(View.INVISIBLE);
    }


    void goHome() {
        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_upload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        doUpload();
    }

    public void doUpload() {
        new DownloadFilesTask().execute();
    }


    private class DownloadFilesTask extends AsyncTask<String, Integer, Long> {
        // Do the long-running work in here
        protected Long doInBackground(String... urls) {
            System.err.println("we : runninggggxcvxcvxcv");
            publishProgress(0);
            /*
            int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
                totalSize += Downloader.downloadFile(urls[i]);
                publishProgress((int) ((i / (float) count) * 100));
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
            return totalSize;
            */

            Intent intent = getIntent();

            RequestBody form = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("officer_id", "" + (intent.getIntExtra("guardId",1) + 1))
                    .addFormDataPart("check_schedule_id", "1")
                    .addFormDataPart("gps_latitude","" + intent.getDoubleExtra("longitude",0.0))
                    .addFormDataPart("gps_longitute", "" + intent.getDoubleExtra("latitude", 0.0))
                    .addFormDataPart("time", "10-Jun-2015")
                    // we need photo file in the future
                    // .addFormDataPart("attack", "10")
                    .build();




            Request request = new Request.Builder()
                    .url("http://wallerpatrol-giver.c9.io/api/forguard/v1/checkin")
                    .post(form)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }

                System.out.println("we : " + response.body().string());


                System.err.println("we : finish finish");
            } catch (Exception e) {
                System.err.println("we : xxx error" + e.toString());
            }

            System.err.println("we : finished?");

            return 1L;
        }

        // This is called each time you call publishProgress()
        protected void onProgressUpdate(Integer... progress) {
            // setProgressPercent(progress[0]);
            tv.setText("กำลังส่งข้อมูล...");
        }

        // This is called when doInBackground() is finished
        protected void onPostExecute(Long result) {
            // showNotification("Downloaded " + result + " bytes");
            tv.setText("ส่งข้อมูลสำเร็จ");
            pb.setVisibility(View.INVISIBLE);
            finishUploadButton.setVisibility(View.VISIBLE);
        }
    }
}


